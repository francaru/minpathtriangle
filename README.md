# MinPathTriangle

MinPathTriangle is a console application written in C#. To execute the application,
navigate to the following path:

    ~/MinPathTriangle/bin/Debug/MinPathTriangle.exe
	
### Requirements

MinPathTriangle uses text files located in the folder called 'trees' on the working 
directory. By default, the folder consists of two files:

* 4-lines.txt
* 50-lines.txt

You can add more files of the same structure in this folder, and they will become
available in the application.
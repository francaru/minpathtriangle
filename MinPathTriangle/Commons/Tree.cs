﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace MinPathTriangle.Commons
{
    /// <summary>
    /// A tree like structure which hosts as a container to a collection of nodes.
    /// </summary>
    public class Tree
    {
        private Node rootNode;
        private List<Node> allNodes;

        /// <summary>
        /// Given a path to a file, this method will load the contents of the
        /// text file and splits them into lines, creating an array of space delimeted
        /// numerals. The numerals are then used to construct a tree using recursive strategy.
        /// </summary>
        /// <param name="path">The path to the file</param>
        /// <returns>An instance of a Tree</returns>
        public static Tree FromFile(string path)
        {
            /*
             * An instance of a Tree is created so that it serves host to all the nodes
             * required for the determination of the Minimum Path.
             */ 
            Tree tree = new Tree();
            tree.allNodes = new List<Node>();

            /*
             * An array of space delimeted numerals is loaded onto memory using .NET's
             * IO library which allows one to load the file one line at a time. Shortly
             * after, a temporary collection is created so that it contains the current
             * nodes being created (here on referred to as 'cursor').
             */
            string[] lines = File.ReadAllLines(path);
            List<Node> cursor = null;

            /*
             * A loop on all the loaded lines is made such that the cursor populates the
             * tree according to the structure denoted in the text file. This operation
             * is therefore the first O(n) problem.
             */
            for (int i = 0; i < lines.Length; i++)
            {
                /*
                 * Given a space delimeted string, the current line will be split into
                 * standalone values by searching for all ' ' in the character sequence.
                 */
                string[] delimetedValues = lines[i].Split(new string[] { " " }, StringSplitOptions.None);

                /*
                 * The following condition checks whether the produced length of the above
                 * split is greater than 1. If the condition is false (meaning the lines
                 * consists of only 1 value), then that value is assigned as the root node
                 * of the tree. Otherwise, the cursor is used to position the nodes accordingly
                 * within the tree.
                 */
                if (delimetedValues.Count() == 1)
                {
                    tree.rootNode = new Node("0_0", null, long.Parse(delimetedValues[0]), new List<Node>(), i);
                    tree.allNodes.Add(tree.rootNode);

                    /*
                     * Starts the cursor with the root node of the tree.
                     */
                    cursor = new List<Node>() { tree.rootNode };
                }
                else
                {
                    /*
                     * In order to avoid reference mutations on the original cursor,
                     * a temporary cursor is created (here on referred to as pointer). 
                     * As opposed to the original cursor, the pointer is populated only
                     * by references to existing nodes. This gives us the advantage of
                     * manipulating the node across different collection (i.e. the pointer
                     * and the original cursor).
                     */
                    List<Node> pointer = new List<Node>();

                    /*
                     * An other loop is used to move horizontally (i.e. to visit all nodes
                     * in the cursor). Assuming the triangle structure is an equilateral triangle,
                     * the time complexity of this loop is exactly equal to the current level (non-zero) 
                     * in the outer loop. 
                     * 
                     * Base Case: Level 1 ∴ No horizontal iterations (root node)            7
                     * Case 1: Level 2 ∴ 2 horizontal iterations                        6       3
                     * Case 2: Level 3 ∴ 3 horizontal iterations                    3       8       5
                     * Case 3: Level 4 ∴ 4 horizontal iterations                11      2       10      9
                     * 
                     * Therefore due to the above explained, the outer loop time complexity
                     * became not faster than O(n^2).
                     */
                    for (int offset = 1; offset <= cursor.Count(); offset++)
                    {
                        /*
                         * Attach a new node as a child to the current cursor. After which
                         * update the pointer only if the node with the current position in
                         * the tree was not already inserted.
                         */
                        Node n1 = new Node(i.ToString() + "_" + (offset - 1).ToString(), cursor[offset - 1], long.Parse(delimetedValues[offset - 1]), new List<Node>(), i);
                        cursor[offset - 1].AddChild(n1);

                        if(UpdateCursor(ref pointer, ref n1))
                        {
                            tree.allNodes.Add(n1);
                        }

                        /*
                         * Attach a second node as a child to the current cursor. After which
                         * update the pointer only if the node with the current position in
                         * the tree was not already inserted.
                         */
                        Node n2 = new Node(i.ToString() + "_" + offset.ToString(), cursor[offset - 1], long.Parse(delimetedValues[offset]), new List<Node>(), i);
                        cursor[offset - 1].AddChild(n2);

                        if (UpdateCursor(ref pointer, ref n2))
                        {
                            tree.allNodes.Add(n2);
                        }
                    }

                    /*
                     * Finally, replace the cursor with the updated pointer.
                     */ 
                    cursor = pointer;
                }
            }

            /*
             * Return the full tree.
             */
            return tree;
        }

        /// <summary>
        /// Mutates the reference of a particular cursor only if the node
        /// being appended is not already included in the same cursor.
        /// </summary>
        /// <param name="cursor">The cursor to update</param>
        /// <param name="node">The node to append if not already exists</param>
        /// <returns>Whether the node was appended to the cursor</returns>
        private static bool UpdateCursor(ref List<Node> cursor, ref Node node)
        {
            if(!cursor.Contains(node))
            {
                cursor.Add(node);
                return true;
            } else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the answer to the Minimum Path by recursively accumulating
        /// the sums of all nodes within the tree, and searches fors the the
        /// least sum amongst the leaf nodes of the tree.
        /// </summary>
        /// <returns>An asycnhronous task which will consist the answer on completion</returns>
        public async Task<Tuple<Node, long>> GetAnswer()
        {
            /*
             * Awaits for the recursive accumulation of the tree starting from the root
             * node to be complete.
             */
            await this.rootNode.Accumulate(0);

            /*
             * The deepest level of nodes in the tree is obtained, with which the
             * all the leaf nodes at the end of the tree are obtained.
             */
            long maxLvl = this.allNodes.Max(n => n.GetLevel());
            IEnumerable<Node> enumeratedNodes = this.allNodes.Where(n => n.GetLevel() == maxLvl);

            /*
             * The minimum sum amongst all the leaf nodes is obtained, and the node containing
             * that particular sum is returned.
             * 
             * Note that FirstOrDefault is being used which will take the first matching node as
             * there could be other cases where a different path leads to the same outcome.
             */
            long minSum = enumeratedNodes.Min(n => n.GetSum());
            Node resultingNode = enumeratedNodes.FirstOrDefault(n => n.GetSum() == minSum);
            return new Tuple<Node, long>(resultingNode, resultingNode.GetSum());
        }

        /// <summary>
        /// Dumps the tree onto the console starting from the root node downwards.
        /// </summary>
        /// <param name="thorough">Whether the dump should include the names of each node (can be slower & less readable for tall trees)</param>
        /// <returns>A dump of the entire tree starting from the root node downwards</returns>
        public string Dump(bool thorough)
        {
            return this.Dump(new List<Node>() { this.rootNode }, thorough);
        }

        /// <summary>
        /// Dumps the tree onto the console starting from a list of nodes.
        /// </summary>
        /// <param name="nodes">The list of node from which the dump should start</param>
        /// <param name="thorough">Whether the dump should include the names of each node (can be slower & less readable for tall trees)</param>
        /// <returns>A dump of the tree starting from specified list of nodes</returns>
        private string Dump(List<Node> nodes, bool thorough)
        {
            string o = "";
            nodes.ForEach(node => {
                if(thorough)
                {
                    o += "(" + node.ToString() + ") " + node.GetValue().ToString() + " ";
                } else
                {
                    o += node.GetValue().ToString() + " ";
                }
            });

            List<Node> next = nodes.SelectMany(x => x.GetChildren()).Distinct().ToList();

            if(next.Count > 0)
            {
                return o + "\n" + this.Dump(next, thorough);
            } else
            {
                return o;
            }
        }

        /// <summary>
        /// A getter for retrieving all nodes.
        /// </summary>
        /// <returns>All nodes in a tree</returns>
        public List<Node> GetAllNodes()
        {
            return this.allNodes;
        }
    }
}

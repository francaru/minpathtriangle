﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace MinPathTriangle.Commons
{
    /// <summary>
    /// A definition of a node which hosts the value for the Minimum Path determination.
    /// </summary>
    public class Node
    {
        #region Variables

        private Node parentNode;
        private string id;
        private long value;
        private long sum;
        private List<Node> childNodes;
        private int level;

        #endregion
        
        /// <summary>
        /// The primary constructor of the Node class.
        /// </summary>
        /// <param name="clusteredIdx">The clustered index of the node (must be unique)</param>
        /// <param name="parentNode">The node from which this node derives</param>
        /// <param name="value">The value of the node</param>
        /// <param name="childNodes">Child nodes to attach to this node instance</param>
        /// <param name="level">The level in which this node is located</param>
        public Node(string clusteredIdx, Node parentNode, long value, List<Node> childNodes, int level)
        {
            this.id = clusteredIdx;
            this.parentNode = parentNode;
            this.value = value;
            this.childNodes = childNodes;
            this.level = level;
        }
        
        /// <summary>
        /// Checks whether this node has children attached to it.
        /// </summary>
        /// <returns>True if this node has children, false otherwise</returns>
        public bool HasChildren()
        {
            return this.childNodes.Count > 0;
        }

        /// <summary>
        /// Gets all the children attached to this node.
        /// </summary>
        /// <returns>All the children attached to this node</returns>
        public List<Node> GetChildren()
        {
            return this.childNodes;
        }

        /// <summary>
        /// Gets the value assigned to this node.
        /// </summary>
        /// <returns>The value assigned to this node</returns>
        public long GetValue()
        {
            return this.value;
        }

        /// <summary>
        /// Attaches a new child to this node.
        /// </summary>
        /// <param name="node">The node to attach</param>
        public void AddChild(Node node)
        {
            this.childNodes.Add(node);
        }

        /// <summary>
        /// Recursively accumualates the sum of this node and its children.
        /// </summary>
        /// <param name="parentSum">The sum of this node's parent</param>
        /// <returns>An asycnhronous task which will dispose itself on completion</returns>
        public async Task Accumulate(long parentSum)
        {
            this.sum = parentSum + this.value;

            /*
             * The following operation will execute parallel accumulators which
             * will recursively traverse downwards the tree from one child to
             * another.
             * 
             * Assuming all node except for the leaf node have 2 children,
             * the following operation will open two parallel forks (one for
             * eac child). Therefore it is safe to say that the complexity of
             * this task in O(logn).
             */
            var tasks = this.childNodes.Select(async childNode =>
            {
                await childNode.Accumulate(this.sum);
            });

            /*
             * Waits for all forks to complete before pushing the result to the
             * parent of the both children.
             */
            await Task.WhenAll(tasks);
        }

        /// <summary>
        /// Recursively gets the path of this node by traversing upwards.
        /// </summary>
        /// <returns>The path of this node</returns>
        public string GetPath()
        {
            if(this.parentNode != null)
            {
                return this.parentNode.GetPath() + " -> " + this.value.ToString();
            } else
            {
                return this.value.ToString();
            }
        }

        /// <summary>
        /// Gets the sum of this node accumulated by its ancestors.
        /// </summary>
        /// <returns>The accumulated sum</returns>
        public long GetSum()
        {
            return this.sum;
        }

        /// <summary>
        /// Gets the level at which this node is located.
        /// </summary>
        /// <returns>The level at which this node located</returns>
        public int GetLevel()
        {
            return this.level;
        }

        /// <summary>
        /// Gets the unique hash code of this node for equality comparison.
        /// </summary>
        /// <returns>The unique hash code of this node</returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Compares this instance against another object.
        /// </summary>
        /// <param name="obj">The other object to which this instance is compared to</param>
        /// <returns>True if the two objects are equal, false otherwise</returns>
        public override bool Equals(object obj)
        {
            return this.GetType().Equals(obj.GetType()) && this.GetHashCode().Equals(obj.GetHashCode());
        }

        /// <summary>
        /// Gives a string representation of this node.
        /// </summary>
        /// <returns>A string representation of this node</returns>
        public override string ToString()
        {
            return "Node " + this.id;
        }
    }
}
﻿using System;
using MinPathTriangle.Commons;
using System.Collections.Generic;
using System.IO;

namespace MinPathTriangle
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] files = Directory.GetFiles("..\\..\\trees");

            Console.WriteLine("Choose the file to build a tree");
            for (int i = 0; i < files.Length; i++)
            {
                Console.WriteLine(String.Format("{0}: {1}", i, files[i]));
            }

            int v = -1;
            while(v < 0 || v > files.Length - 1)
            {
                try
                {
                    v = int.Parse(Console.ReadLine());
                }
                catch
                {
                    v = -1;
                }

                if(v < 0 || v > files.Length - 1)
                {
                    Console.WriteLine("Invalid selection. Please choose an option from 0 to " + (files.Length - 1));
                }
            }

            Console.Clear();
            Tree tree = Tree.FromFile(files[v]);

            Console.WriteLine(tree.Dump(false));
            Console.WriteLine();

            var response = tree.GetAnswer().Result;
            Console.WriteLine("Minimal path is: " + response.Item1.GetPath() + " = " + response.Item2.ToString());

            Console.ReadKey();
        }
    }
}